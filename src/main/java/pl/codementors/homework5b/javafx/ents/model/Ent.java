package pl.codementors.homework5b.javafx.ents.model;

import java.io.Serializable;

public class Ent implements Serializable {

    private String species;
    private int year;
    private int height;

    private Type type;

    public enum Type {CONIFEROUS, LEAFY}

    public Ent(String species, int year, int height, Type type) {
        this.species = species;
        this.year = year;
        this.height = height;
        this.type = type;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Ent() {

    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
