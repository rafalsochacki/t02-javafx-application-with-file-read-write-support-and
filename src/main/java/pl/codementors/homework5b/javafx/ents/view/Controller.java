package pl.codementors.homework5b.javafx.ents.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.ChoiceBoxTableCell;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.Region;
import javafx.stage.FileChooser;
import javafx.util.converter.IntegerStringConverter;
import pl.codementors.homework5b.javafx.ents.model.Ent;
import pl.codementors.homework5b.javafx.ents.workers.OpenWorker;
import pl.codementors.homework5b.javafx.ents.workers.SaveWorker;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Logger;

public class Controller implements Initializable {

    private static final Logger log = Logger.getLogger(Controller.class.getCanonicalName());

    @FXML
    private TableView entsTable;


    @FXML
    private TableColumn<Ent, String> species;

    @FXML
    private TableColumn<Ent, Integer> year;

    @FXML
    private TableColumn<Ent, Integer> height;

    @FXML
    private TableColumn<Ent, Ent.Type> type;

    @FXML
    private ProgressBar progress;

    private ObservableList<Ent> ents = FXCollections.observableArrayList();
    private ResourceBundle rb;

    public Controller() {
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        entsTable.setItems(ents);
        rb = resourceBundle;

        progress.setProgress(0);

        species.setCellFactory(TextFieldTableCell.forTableColumn());
        species.setOnEditCommit(event -> event.getRowValue().setSpecies(event.getNewValue()));

        year.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        year.setOnEditCommit(event -> event.getRowValue().setYear(event.getNewValue()));

        height.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        height.setOnEditCommit(event -> event.getRowValue().setHeight(event.getNewValue()));

        type.setCellFactory(ChoiceBoxTableCell.forTableColumn(Ent.Type.values()));
        type.setOnEditCommit(event -> event.getRowValue().setType(event.getNewValue()));
    }

    @FXML
    private void open(ActionEvent event) {
        FileChooser chooser = new FileChooser();
        File file = chooser.showOpenDialog(((MenuItem) event.getTarget()).getParentPopup().getScene().getWindow());
        if (file != null) {
            open(file);
        }
    }

    private void open(File file) {
        OpenWorker worker = new OpenWorker(ents, file);
        progress.progressProperty().bind(worker.progressProperty());
        new Thread(worker).start();
    }

    @FXML
    private void about(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(rb.getString("aboutTitle"));
        alert.setHeaderText(rb.getString("aboutHeader"));
        alert.setContentText(rb.getString("aboutContent"));
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        alert.showAndWait();
    }

    @FXML
    private void save(ActionEvent event) {
        FileChooser chooser = new FileChooser();
        File file = chooser.showSaveDialog(((MenuItem) event.getTarget()).getParentPopup().getScene().getWindow());
        if (file != null) {
            save(file);
        }
    }

    private void save(File file) {
        SaveWorker worker = new SaveWorker(ents, file);
        progress.progressProperty().bind(worker.progressProperty());
        new Thread(worker).start();
    }

    @FXML
    private void plant(ActionEvent event) {
        ents.add(new Ent());
    }

    @FXML
    private void cut(ActionEvent event) {
        if (entsTable.getSelectionModel().getSelectedIndex() >= 0) {
            ents.remove(entsTable.getSelectionModel().getSelectedIndex());
            entsTable.getSelectionModel().clearSelection();
        }
    }

    @FXML
    private void rootTheSeedling(ActionEvent event) {
        Object selectedEnt = entsTable.getSelectionModel().getSelectedItem();
        Ent newEnt = Ent.class.cast(selectedEnt);
        ents.add(new Ent(newEnt.getSpecies(), 2018, 10, newEnt.getType()));

    }
}

