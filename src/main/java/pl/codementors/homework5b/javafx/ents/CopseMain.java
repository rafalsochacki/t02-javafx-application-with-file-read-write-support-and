package pl.codementors.homework5b.javafx.ents;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class CopseMain extends Application {
    Stage window;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        window = stage;

        URL fxml = this.getClass().getResource("/pl/codementors/homework5b/javafx/ents/view/ents_list.fxml");
        ResourceBundle rb = ResourceBundle.getBundle("pl.codementors.homework5b.javafx.ents.view.messages.ents_list_msg");

        Parent root = FXMLLoader.load(fxml, rb);

        Scene scene = new Scene(root, 800, 600);
        scene.getStylesheets().add("/pl/codementors/homework5b/javafx/ents/view/css/ents_list.css");
        window.setTitle(rb.getString("applicationTitle"));
        window.setScene(scene);
        window.show();
    }
}
