package pl.codementors.homework5b.javafx.ents.workers;

import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.concurrent.Task;
import pl.codementors.homework5b.javafx.ents.model.Ent;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SaveWorker extends Task<Collection<Ent>> {

    private static final Logger log = Logger.getLogger(SaveWorker.class.getCanonicalName());


    private Collection<Ent> ents;


    private File file;

    private DoubleProperty progress = new SimpleDoubleProperty(0);

    public SaveWorker(Collection<Ent> ents, File file) {
        this.ents = ents;
        this.file = file;
    }

    @Override
    protected Collection<Ent> call() throws Exception {
        int i = 0;
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(file))) {
            objectOutputStream.writeObject(ents.size());
            for (Ent ent : ents) {
                try {
                    objectOutputStream.writeObject(ent);
                    Thread.sleep(500);//Only to see that something is happening.
                    Platform.runLater(() -> progress.set((double) i / ents.size()));
                    updateProgress(i + 1, i);
                } catch (InterruptedException ex) {
                    log.log(Level.WARNING, ex.getMessage(), ex);
                }
            }
        } catch (IOException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
        return null;
    }

}
